from Controllers.main_controller import MainController
from multiprocessing import Process

def initialise_new_session():
    session_controller = MainController()
    session_controller.run()

if __name__ == '__main__':
    p = Process(target=initialise_new_session, args = ())
    p.start()
    p.join()

