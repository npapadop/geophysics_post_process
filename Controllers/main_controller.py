from tkinter import *
from tkinter import filedialog
from Models.model import Model
from Views.main_view import MainView
from Views.delete_sensor_view import DeleteSensorView
from Views.clip_track_data_view import ClipTrackView
from Views.outlier_removal_view import OutlierRemovalView
from Views.processing_view import ProcessingView
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from Views.custor_figure_buttons_toolbar import CustomButtonsToolbar
from matplotlib.figure import Figure
import matplotlib.mlab as mlab
import time
import threading
import os

APPLICATION_NAME = 'GPP Sensys Multiarray'


class MainController:
    def __init__(self):
        self.root = Tk()
        self.model = Model()
        self.main_view = MainView(self.root)

        self.data_file_path = None

        #initialize views
        self.initialise_view_components()

        self.center_root_position()

        #initialise button bindings
        self.initialise_button_bindings()

        #initialise remove points progress observable
        self.model.remove_points_progress_perc.addCallback(self.remove_points_progress_changed)
        self.console_message_current_line = 0

    def run(self):
        self.root.title(APPLICATION_NAME)
        self.root.resizable(TRUE, TRUE)
        self.root.rowconfigure(0, weight=1)
        self.root.columnconfigure(0, weight=1)
        self.root.mainloop()

    def initialise_view_components(self):
        self.main_view.tracks_stats_label.config(text="n/a")
        self.main_view.records_stats_label.config(text="n/a")
        self.main_view.min_stats_label.config(text="n/a")
        self.main_view.max_stats_label.config(text="n/a")
        self.main_view.mean_stats_label.config(text="n/a")
        self.main_view.median_stats_label.config(text="n/a")
        self.main_view.std_stats_label.config(text="n/a")
        self.main_view.var_stats_label.config(text="n/a")
        self.root.update_idletasks()

    def initialise_button_bindings(self):
        #Bindings
        self.enable_button_binding(self.main_view.exit_button, self.exit_button_clicked)
        self.enable_button_binding(self.main_view.clear_log_button, self.clear_log_button_clicked)
        self.enable_button_binding(self.main_view.load_data_button, self.load_data_button_clicked)
        self.disable_button_binding(self.main_view.reload_data_button)
        self.disable_button_binding(self.main_view.export_data_button)
        self.disable_button_binding(self.main_view.calibrate_sensor_button_all)
        self.disable_button_binding(self.main_view.calibrate_sensor_button_all_tracks)
        self.disable_button_binding(self.main_view.calibrate_sensor_button_each_track)
        self.disable_button_binding(self.main_view.delete_sensor_button)
        self.disable_button_binding(self.main_view.clip_track_button)
        self.disable_button_binding(self.main_view.outlier_removal_button)
        self.disable_button_binding(self.main_view.remove_overlap_button)
        self.disable_button_binding(self.main_view.plot_outlines_button)

    def enable_buttons_on_load(self):
        self.enable_button_binding(self.main_view.reload_data_button, self.reload_data_button_clicked)
        self.enable_button_binding(self.main_view.export_data_button, self.export_data_button_clicked)
        self.enable_button_binding(self.main_view.calibrate_sensor_button_all, self.calibrate_sensor_button_all_clicked)
        self.enable_button_binding(self.main_view.calibrate_sensor_button_all_tracks, self.calibrate_sensor_button_all_tracks_clicked)
        self.enable_button_binding(self.main_view.calibrate_sensor_button_each_track,
                                   self.calibrate_sensor_button_each_track_clicked)
        self.enable_button_binding(self.main_view.delete_sensor_button, self.delete_sensor_button_clicked)
        self.enable_button_binding(self.main_view.clip_track_button, self.clip_track_button_clicked)
        self.enable_button_binding(self.main_view.outlier_removal_button, self.outlier_removal_button_clicked)
        self.enable_button_binding(self.main_view.remove_overlap_button, self.remove_overlap_button_clicked)
        self.enable_button_binding(self.main_view.plot_outlines_button, self.plot_outlines_button_clicked)

    def clear_log_button_clicked(self, event):
        self.main_view.console_text.configure(state='normal')
        self.main_view.console_text.delete(1.0, END)
        self.main_view.console_text.configure(state='disabled')
        self.main_view.console_text.update()

    def load_data_button_clicked(self, event):

        file_path = filedialog.askopenfilename(defaultextension=".asc", filetypes=[("ASCII Files", "*.asc")])
        if not file_path:
            return

        file_name = os.path.split(file_path)[-1]

        self.main_view.data_stats_label.config(text="Data Statistics: {0} dataset".format(file_name))
        self.add_console_message("Reading data from file: {0} ...".format(file_path))
        self.model.open_file(file_path, file_name)
        self.add_console_message("Data loaded successfully")
        self.update_stats()
        self.enable_buttons_on_load()

    def reload_data_button_clicked(self, event):
        if len(self.model.tracks) == 0:
            self.add_console_message("No data exist. Load initial data")
            return

        self.add_console_message("Reloading initial data...")
        self.model.reload_initial_data()
        self.add_console_message("Initial data reloaded")
        self.update_stats()
        self.enable_buttons_on_load()

    def export_data_button_clicked(self, event):
        if len(self.model.tracks) == 0:
            self.add_console_message("No data exist. Load initial data")
            return

        file_path = filedialog.asksaveasfilename(defaultextension=".asc", filetypes=[("ASCII Files", "*.asc")])
        if not file_path:
            return

        self.add_console_message("Saving data to file: %s..." %file_path)
        self.model.write_track_data_from_planar(file_path)
        self.add_console_message("Save data completed")

    def update_stats(self):
        self.add_console_message("Updating data statistics...")
        self.model.calculate_data_stats()
        self.update_stats_labels()
        self.draw_stats_histogram()
        self.add_console_message("Data statistics updated")

    def draw_stats_histogram(self):

        for widget in self.main_view.histogram_stats_frame.winfo_children():
            widget.destroy()

        mu = self.model.stats_mean
        sigma = self.model.stats_std

        fig = Figure()
        ax = fig.add_subplot(111)

        n, bins, patches = ax.hist(self.model.sensor_values, bins='sqrt', facecolor='green', alpha=0.75)
        bincenters = 0.5 * (bins[1:] + bins[:-1])
        y = mlab.normpdf(bincenters, mu, sigma)
        #l = ax.plot(bincenters, y, 'r--', linewidth=1)

        ax.set_xlabel('Sensor Values')
        ax.set_ylabel('Frequency')
        ax.format_coord = lambda x, y: ''
        ax.axis(xmin=(self.model.stats_mean-3*self.model.stats_std), xmax=(self.model.stats_mean+3*self.model.stats_std))
        ax.set_title(r'Histogram: avg={0:.3f}, std={1:.3f}'.format(mu, sigma))
        ax.grid(True)

        canvas = FigureCanvasTkAgg(fig, master=self.main_view.histogram_stats_frame)
        canvas.show()
        canvas.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)

        toolbar = CustomButtonsToolbar(canvas, self.main_view.histogram_stats_frame)
        toolbar.update()

    def update_stats_labels(self):
        self.main_view.tracks_stats_label.config(text='{0:d}'.format(self.model.num_tracks))
        self.main_view.records_stats_label.config(text='{0:d}'.format(self.model.num_records))
        self.main_view.min_stats_label.config(text='{0:.2f}'.format(self.model.stats_amin))
        self.main_view.max_stats_label.config(text='{0:.2f}'.format(self.model.stats_amax))
        self.main_view.mean_stats_label.config(text='{0:.2f}'.format(self.model.stats_mean))
        self.main_view.median_stats_label.config(text='{0:.2f}'.format(self.model.stats_median))
        self.main_view.std_stats_label.config(text='{0:.2f}'.format(self.model.stats_std))
        self.main_view.var_stats_label.config(text='{0:.2f}'.format(self.model.stats_var))

    def add_console_message(self, console_message):
        curtime = time.strftime("%H:%M:%S", time.localtime())
        self.main_view.console_text.configure(state='normal')
        self.main_view.console_text.insert(END, "[%s] %s\n" % (curtime, console_message))
        self.main_view.console_text.yview(END)
        self.main_view.console_text.configure(state='disabled')
        self.main_view.console_text.update()
        self.console_message_current_line += 1

    def delete_sensor_button_clicked(self, event):
        if len(self.model.tracks) == 0:
            self.add_console_message("No data exist. Load initial data")
            return

        self.delete_sensor_widget = Toplevel()
        self.delete_sensor_widget.resizable(0,0)
        self.delete_sensor_widget.title("Delete sensor data")
        self.delete_sensor_form = DeleteSensorView(self.delete_sensor_widget)

        self.delete_sensor_form.sensor_option_list = list(range(1, len(self.model.tracks[0])+1, 1))
        self.delete_sensor_form.track_option_list = ['all', 'odd', 'even']
        self.delete_sensor_form.create_form_widget()

        self.delete_sensor_form.cancel_button.bind("<ButtonRelease-1>", self.sensor_form_cancel_clicked)
        self.delete_sensor_form.apply_button.bind("<ButtonRelease-1>", self.sensor_form_apply_clicked)

        self.center_widget_position(self.delete_sensor_widget)

        self.delete_sensor_widget.transient(self.root)
        self.delete_sensor_widget.grab_set()
        self.root.wait_window(self.delete_sensor_widget)

    def sensor_form_cancel_clicked(self, event):
        self.delete_sensor_widget.destroy()

    def sensor_form_apply_clicked(self, event):
        self.delete_sensor_widget.destroy()
        self.add_console_message("Deleting sensor#%d values from %s tracks..." %(self.delete_sensor_form.sensor_list_choise.get(), self.delete_sensor_form.track_list_choise.get()))
        self.model.delete_sensor(self.delete_sensor_form.sensor_list_choise.get(), self.delete_sensor_form.track_list_choise.get())
        self.add_console_message("Sensor values deleted")
        self.update_stats()

    def clip_track_button_clicked(self, event):
        if len(self.model.tracks) == 0:
            self.add_console_message("No data exist. Load initial data")
            return

        self.clip_track_widget = Toplevel()
        self.clip_track_widget.resizable(0, 0)
        self.clip_track_widget.title("Clip track data")
        self.clip_track_form = ClipTrackView(self.clip_track_widget)

        self.clip_track_form.maxmodelvalue = self.model.stats_amax
        self.clip_track_form.minmodelvalue = self.model.stats_amin
        self.clip_track_form.minimum_value_str = StringVar(self.clip_track_widget, value='-50')
        self.clip_track_form.maximum_value_str = StringVar(self.clip_track_widget, value='50')

        self.clip_track_form.create_form_widget()

        self.clip_track_form.error_minimum_label.config(text='')
        self.clip_track_form.error_maximum_label.config(text='')

        self.clip_track_form.cancel_button.bind("<ButtonRelease-1>", self.clip_track_form_cancel_clicked)
        self.clip_track_form.apply_button.bind("<ButtonRelease-1>", self.clip_track_form_apply_clicked)

        self.center_widget_position(self.clip_track_widget)

        self.clip_track_widget.transient(self.root)
        self.clip_track_widget.grab_set()
        self.root.wait_window(self.clip_track_widget)

    def clip_track_form_cancel_clicked(self, event):
        self.clip_track_widget.destroy()

    def clip_track_form_apply_clicked(self, event):
        found_error = False
        self.clip_track_form.error_minimum_label.config(text='')
        self.clip_track_form.error_maximum_label.config(text='')
        min_str = self.clip_track_form.minimum_value_str.get()
        max_str = self.clip_track_form.maximum_value_str.get()

        #check empty
        if len(min_str) == 0:
            self.clip_track_form.error_minimum_label.config(text='Enter minimum value')
            found_error = True
        if len(max_str) == 0:
            self.clip_track_form.error_maximum_label.config(text='Enter maximum value')
            found_error = True
        if found_error:
            return


        #check float
        try:
            self.model.clip_track_min = float(min_str)
        except ValueError:
            self.clip_track_form.error_minimum_label.config(text='Enter a numeric value')
            found_error = True

        try:
            self.model.clip_track_max = float(max_str)
        except ValueError:
            self.clip_track_form.error_maximum_label.config(text='Enter a numeric value')
            found_error = True
        if found_error:
            return

        if self.model.clip_track_min > self.model.clip_track_max:
            self.clip_track_form.error_minimum_label.config(text='Min value is greater than maximum clip limit')
            return

        self.clip_track_widget.destroy()
        self.add_console_message("Clipping track data values using min: %.1f and max: %.1f limits" %(self.model.clip_track_min, self.model.clip_track_max))
        self.model.clip_track()
        self.add_console_message("Clipping track values completed")
        self.update_stats()

    def outlier_removal_button_clicked(self, event):
        if len(self.model.tracks) == 0:
            self.add_console_message("No data exist. Load initial data")
            return

        self.outlier_removal_widget = Toplevel()
        self.outlier_removal_widget.resizable(0, 0)
        self.outlier_removal_widget.title("Clip track data")
        self.outlier_removal_form = OutlierRemovalView(self.outlier_removal_widget)

        self.outlier_removal_form.adjustment_value_str = StringVar(self.outlier_removal_widget, value='2.5')

        self.outlier_removal_form.create_form_widget()

        self.outlier_removal_form.error_parameter_label.config(text='')

        self.outlier_removal_form.cancel_button.bind("<ButtonRelease-1>", self.outlier_removal_form_cancel_clicked)
        self.outlier_removal_form.apply_button.bind("<ButtonRelease-1>", self.outlier_removal_form_apply_clicked)

        self.center_widget_position(self.outlier_removal_widget)

        self.outlier_removal_widget.transient(self.root)
        self.outlier_removal_widget.grab_set()
        self.root.wait_window(self.outlier_removal_widget)

    def outlier_removal_form_cancel_clicked(self, event):
        self.outlier_removal_widget.destroy()

    def outlier_removal_form_apply_clicked(self, event):
        self.outlier_removal_form.error_parameter_label.config(text='')

        adjustment_param_str = self.outlier_removal_form.adjustment_value_str.get()

        #check empty
        if len(adjustment_param_str) == 0:
            self.outlier_removal_form.error_parameter_label.config(text='Enter parameter value')
            return

        #check float
        try:
            self.model.adjustment_parameter = float(adjustment_param_str)
        except ValueError:
            self.outlier_removal_form.error_parameter_label.config(text='Enter a numeric value')
            return

        self.outlier_removal_widget.destroy()
        self.add_console_message("Starting outlier removal processing...")
        self.model.outlier_removal()
        self.add_console_message("Outlier removal completed")
        self.update_stats()

    # def calibrate_sensor_button_clicked(self, event):
    #     if len(self.model.tracks) == 0:
    #         self.add_console_message("No data exist. Load initial data")
    #         return
    #
    #     self.add_console_message("Calibrating sensor values")
    #     self.model.calibrate_sensor_values()
    #     self.add_console_message("Sensor values calibrated successfully")
    #     self.update_stats()
    #
    #     self.disable_button_binding(self.main_view.calibrate_sensor_button)

    def calibrate_sensor_button_all_clicked(self, event):
        if len(self.model.tracks) == 0:
            self.add_console_message("No data exist. Load initial data")
            return

        self.add_console_message("Calibrating all sensor values on all tracks")
        self.process_widget = Toplevel()
        self.process_widget.resizable(0, 0)
        self.process_widget.title("Calibrating Progress")
        self.process_form = ProcessingView(self.process_widget)
        self.process_form.progress_label.config(text=self.model.get_progress_bar_str(0, len(self.model.tracks), length=50))
        self.model.calibrate_values_progress_perc.addCallback(self.calibrate_progress_changed)
        self.enable_button_binding(self.process_form.cancel_process_button, self.cancel_calibrating_values)
        self.process_widget.protocol("WM_DELETE_WINDOW", lambda: self.cancel_remove_overlapping_points(event=None))
        self.process_widget.update()
        self.center_widget_position(self.process_widget)

        self.process_thread = threading.Thread(target=self.model.calibrate_sensor_values_all, args=(0, self.callback_when_calibration_done))
        self.process_thread.start()

        self.process_widget.transient(self.root)
        self.process_widget.grab_set()
        self.root.wait_window(self.process_widget)

    def calibrate_sensor_button_all_tracks_clicked(self, event):
        if len(self.model.tracks) == 0:
            self.add_console_message("No data exist. Load initial data")
            return

        self.add_console_message("Calibrating each sensor values on all tracks")
        self.process_widget = Toplevel()
        self.process_widget.resizable(0, 0)
        self.process_widget.title("Calibrating Progress")
        self.process_form = ProcessingView(self.process_widget)
        self.process_form.progress_label.config(text=self.model.get_progress_bar_str(0, len(self.model.tracks), length=50))
        self.model.calibrate_values_progress_perc.addCallback(self.calibrate_progress_changed)
        self.enable_button_binding(self.process_form.cancel_process_button, self.cancel_calibrating_values)
        self.process_widget.protocol("WM_DELETE_WINDOW", lambda: self.cancel_remove_overlapping_points(event=None))
        self.process_widget.update()
        self.center_widget_position(self.process_widget)

        self.process_thread = threading.Thread(target=self.model.calibrate_sensor_values_all_tracks, args=(0, self.callback_when_calibration_done))
        self.process_thread.start()

        self.process_widget.transient(self.root)
        self.process_widget.grab_set()
        self.root.wait_window(self.process_widget)

    def calibrate_sensor_button_each_track_clicked(self, event):
        if len(self.model.tracks) == 0:
            self.add_console_message("No data exist. Load initial data")
            return

        self.add_console_message("Calibrating each sensor values on each track")
        self.process_widget = Toplevel()
        self.process_widget.resizable(0, 0)
        self.process_widget.title("Calibrating Progress")
        self.process_form = ProcessingView(self.process_widget)
        self.process_form.progress_label.config(text=self.model.get_progress_bar_str(0, len(self.model.tracks), length=50))
        self.model.calibrate_values_progress_perc.addCallback(self.calibrate_progress_changed)
        self.enable_button_binding(self.process_form.cancel_process_button, self.cancel_calibrating_values)
        self.process_widget.protocol("WM_DELETE_WINDOW", lambda: self.cancel_remove_overlapping_points(event=None))
        self.process_widget.update()
        self.center_widget_position(self.process_widget)

        self.process_thread = threading.Thread(target=self.model.calibrate_sensor_values_each_track, args=(0, self.callback_when_calibration_done))
        self.process_thread.start()

        self.process_widget.transient(self.root)
        self.process_widget.grab_set()
        self.root.wait_window(self.process_widget)

    def callback_when_calibration_done(self):
        self.process_widget.destroy()
        self.add_console_message("Sensor values calibrated successfully")
        self.update_stats()

    def calibrate_progress_changed(self, progress):
        self.process_form.progress_label.config(text=progress)

    def cancel_calibrating_values(self, event):
        self.model.process_calibrate_values_terminated = 1
        self.model.calibrate_values_progress_perc.delCallback(self.calibrate_progress_changed)
        self.disable_button_binding(self.main_view.export_data_button)
        self.disable_button_binding(self.main_view.calibrate_sensor_button_all)
        self.disable_button_binding(self.main_view.calibrate_sensor_button_all_tracks)
        self.disable_button_binding(self.main_view.calibrate_sensor_button_each_track)
        self.disable_button_binding(self.main_view.delete_sensor_button)
        self.disable_button_binding(self.main_view.clip_track_button)
        self.disable_button_binding(self.main_view.outlier_removal_button)
        self.disable_button_binding(self.main_view.remove_overlap_button)
        self.disable_button_binding(self.main_view.plot_outlines_button)

        self.process_widget.destroy()
        self.add_console_message("Calibrating sensor values process canceled")
        self.add_console_message("Please reinitialise data to proceed")

    def remove_overlap_button_clicked(self, event):
        if len(self.model.tracks) == 0:
            self.add_console_message("No data exist. Load initial data")
            return

        if len(self.model.tracks) == 1:
            self.add_console_message("Cannot remove overlap points. Only one track exists")
            return

        # create outline polygons
        self.add_console_message("Start creating outline polygons...")
        self.model.create_outline_polygons()
        self.add_console_message("Outline polygons created...")

        # find ovellaping tracks
        self.add_console_message("Start creating outline convex hulls...")
        self.model.create_outline_convexhulls()
        self.add_console_message("Creating outline convex hulls completed")

        # find ovellaping tracks
        self.add_console_message("Start locating overlapping tracks...")
        self.model.locate_ovelapping_tracks()
        self.add_console_message("Locating overlapping tracks completed")

        self.add_console_message("Start removing overlapping points...")
        self.process_widget = Toplevel()
        self.process_widget.resizable(0, 0)
        self.process_widget.title("Progress")
        self.process_form = ProcessingView(self.process_widget)
        self.process_form.progress_label.config(
            text=self.model.get_progress_bar_str(0, len(self.model.polygon_intersect_indexes), length=50))
        self.model.remove_points_progress_perc.addCallback(self.remove_points_progress_changed)
        self.enable_button_binding(self.process_form.cancel_process_button, self.cancel_remove_overlapping_points)
        self.process_widget.protocol("WM_DELETE_WINDOW", lambda: self.cancel_remove_overlapping_points(event=None))
        self.process_widget.update()
        self.center_widget_position(self.process_widget)

        self.process_thread = threading.Thread(target=self.model.remove_ovelapping_points,
                                               args=(0, self.callback_when_done))
        self.process_thread.start()

        self.process_widget.transient(self.root)
        self.process_widget.grab_set()
        self.root.wait_window(self.process_widget)

    def cancel_remove_overlapping_points(self, event):
        self.model.process_remove_points_terminated = 1
        self.model.remove_points_progress_perc.delCallback(self.remove_points_progress_changed)
        self.disable_button_binding(self.main_view.export_data_button)
        self.disable_button_binding(self.main_view.calibrate_sensor_button_all)
        self.disable_button_binding(self.main_view.calibrate_sensor_button_all_tracks)
        self.disable_button_binding(self.main_view.calibrate_sensor_button_each_track)
        self.disable_button_binding(self.main_view.delete_sensor_button)
        self.disable_button_binding(self.main_view.clip_track_button)
        self.disable_button_binding(self.main_view.outlier_removal_button)
        self.disable_button_binding(self.main_view.remove_overlap_button)
        self.disable_button_binding(self.main_view.plot_outlines_button)

        self.process_widget.destroy()
        self.add_console_message("Removing overlapping points process canceled")
        self.add_console_message("Please reinitialise data to proceed")

    def callback_when_done(self):
        self.process_widget.destroy()
        self.disable_button_binding(self.main_view.remove_overlap_button)
        self.disable_button_binding(self.main_view.plot_outlines_button)
        self.add_console_message("Removing overlapping points completed")

    def remove_points_progress_changed(self, progress):
        # self.main_view.console_text.configure(state='normal')
        # self.main_view.console_text.delete("insert linestart", "insert lineend")
        # self.main_view.console_text.insert("insert linestart", progress)
        # self.main_view.console_text.configure(state='disabled')
        # self.main_view.console_text.update()
        self.process_form.progress_label.config(text=progress)

    def plot_outlines_button_clicked(self,event):
        if len(self.model.tracks) == 0:
            self.add_console_message("No data exist. Load initial data")
            return

        self.model.plot_outline_polygons()

    def enable_button_binding(self, button_widget, binding_func):
        button_widget.config(state='normal')
        button_widget.bind("<ButtonRelease-1>", binding_func)

    def disable_button_binding(self, button_widget):
        button_widget.config(state='disabled')
        button_widget.unbind("<ButtonRelease-1>")

    def center_widget_position(self, widget_v):
        x = self.root.winfo_screenwidth() / 2 - widget_v.winfo_reqwidth() / 2
        y = self.root.winfo_screenheight() / 2 - widget_v.winfo_reqheight() / 2
        widget_v.geometry("+%d+%d" % (x, y))

    def center_root_position(self):
        x = self.root.winfo_screenwidth() / 2 - self.root.winfo_width()/2
        y = self.root.winfo_screenheight() / 2 - self.root.winfo_height()/2
        self.root.geometry("+%d+%d" % (x, y))

    def exit_button_clicked(self, event):
        self.root.destroy()
