from tkinter import *


class ProcessingView:
    def __init__(self, parentframe):
        self.processing_form_frame = Frame(parentframe)
        self.processing_form_frame.pack()
        Label(self.processing_form_frame, text='Processing please wait...').grid(row=0, column=0, columnspan=5, sticky=W + E + N + S, padx=10, pady=10)
        self.progress_label = Label(self.processing_form_frame)
        self.progress_label.grid(row=1, column=0, columnspan=5, sticky=W + E + N + S, padx=10, pady=10)
        self.cancel_process_button = Button(self.processing_form_frame, text="Cancel")
        self.cancel_process_button.grid(row=2, column=2, sticky=W+E+N+S, padx=10, pady=10)

