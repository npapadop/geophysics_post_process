from tkinter import *

class MainView:
    def __init__(self, parentview):
        self.parentview = parentview
        self.create_main_gui()

    def create_main_gui(self):
        self.root_frame = Frame(self.parentview)
        self.root_frame.rowconfigure(0, weight=3)
        self.root_frame.columnconfigure(0, weight=0)
        self.root_frame.rowconfigure(1, weight=1)
        self.root_frame.columnconfigure(1, weight=1)
        self.root_frame.grid(sticky=N+S+E+W)

        self.create_sidebar_frame(self.root_frame)
        self.create_stats_frame(self.root_frame)
        self.create_output_frame(self.root_frame)

    def create_sidebar_frame(self, parent):
        self.sidebar_frame = Frame(parent, bd=1, relief=GROOVE)
        self.sidebar_frame.grid(row=0, column=0, rowspan=2, sticky=N+S)

        data_buttons_group = LabelFrame(self.sidebar_frame, text="Load/Export Data", padx=10, pady=10)
        data_buttons_group.grid(row=0, padx=10, pady=10, sticky=E+W)
        self.load_data_button = Button(data_buttons_group, text='Load Data')
        self.load_data_button.pack(padx=5, pady=5, fill=X)
        self.reload_data_button = Button(data_buttons_group, text='Reload Initial Data')
        self.reload_data_button.pack(padx=5, pady=5, fill=X)
        self.export_data_button = Button(data_buttons_group, text='Export Data')
        self.export_data_button.pack(padx=5, pady=5, fill=X)

        data_buttons_group = LabelFrame(self.sidebar_frame, text="Calibrate Data", padx=10, pady=10)
        data_buttons_group.grid(row=1, padx=10, pady=10, sticky=E+W)
        self.calibrate_sensor_button_all = Button(data_buttons_group, text='Calibrate All Sensors on All Tracks')
        self.calibrate_sensor_button_all.pack(padx=5, pady=5, fill=X)
        self.calibrate_sensor_button_all_tracks = Button(data_buttons_group, text='Calibrate Each Sensor on All Tracks')
        self.calibrate_sensor_button_all_tracks.pack(padx=5, pady=5, fill=X)
        self.calibrate_sensor_button_each_track = Button(data_buttons_group, text='Calibrate Each Sensor on Each Track')
        self.calibrate_sensor_button_each_track.pack(padx=5, pady=5, fill=X)

        data_buttons_group = LabelFrame(self.sidebar_frame, text="Process Data", padx=10, pady=10)
        data_buttons_group.grid(row=2, padx=10, pady=10, sticky=E+W)
        self.delete_sensor_button = Button(data_buttons_group, text='Delete Sensor Data')
        self.delete_sensor_button.pack(padx=5, pady=5, fill=X)
        self.clip_track_button = Button(data_buttons_group, text='Clip Track Data')
        self.clip_track_button.pack(padx=5, pady=5, fill=X)
        self.outlier_removal_button = Button(data_buttons_group, text='Outlier Removal')
        self.outlier_removal_button.pack(padx=5, pady=5, fill=X)
        self.remove_overlap_button = Button(data_buttons_group, text='Remove Overlapping Points')
        self.remove_overlap_button.pack(padx=5, pady=5, fill=X)

        data_buttons_group = LabelFrame(self.sidebar_frame, text="Data Representation", padx=10, pady=10)
        data_buttons_group.grid(row=3, padx=10, pady=10, sticky=E+W)
        self.plot_outlines_button = Button(data_buttons_group, text='Plot Tracks')
        self.plot_outlines_button.pack(padx=5, pady=5, fill=X)

        self.exit_button = Button(self.sidebar_frame, text="Exit")
        self.exit_button.grid(row=4, padx=25, pady=25, sticky=E+W)

    def create_stats_frame(self, parent):
        stats_frame = Frame(parent, bd=1, relief=GROOVE)
        stats_frame.rowconfigure(0, weight=0)
        stats_frame.rowconfigure(1, weight=1)
        stats_frame.columnconfigure(0, weight=0)
        stats_frame.columnconfigure(1, weight=1)
        stats_frame.grid(row=0, column=1, sticky=W+N+S+E)
        self.data_stats_label = Label(stats_frame, text='Data Statistics')
        self.data_stats_label.grid(row=0, columnspan=2, padx=10, pady=10, sticky=W)

        label_stats_frame = Frame(stats_frame, bd=0, relief=GROOVE)
        label_stats_frame.grid(row=1, column=0, sticky=W+N+S+E)
        Label(label_stats_frame, text='Tracks:').grid(row=0, column=0, padx=(10, 0), pady=5, sticky=W)
        self.tracks_stats_label = Label(label_stats_frame)
        self.tracks_stats_label.grid(row=0, column=1, padx=(0, 10), pady=5, sticky=W)
        Label(label_stats_frame, text='Records:').grid(row=1, column=0, padx=(10, 0), pady=5, sticky=W)
        self.records_stats_label = Label(label_stats_frame)
        self.records_stats_label.grid(row=1, column=1, padx=(0, 10), pady=5, sticky=W)
        Label(label_stats_frame, text='Min:').grid(row=2, column=0, padx=(10, 0), pady=5, sticky=W)
        self.min_stats_label = Label(label_stats_frame)
        self.min_stats_label.grid(row=2, column=1, padx=(0, 10), pady=5, sticky=W)
        Label(label_stats_frame, text='Max:').grid(row=3, column=0, padx=(10, 0), pady=5, sticky=W)
        self.max_stats_label = Label(label_stats_frame)
        self.max_stats_label.grid(row=3, column=1, padx=(0, 10), pady=5, sticky=W)
        Label(label_stats_frame, text='Mean:').grid(row=4, column=0, padx=(10, 0), pady=5, sticky=W)
        self.mean_stats_label = Label(label_stats_frame)
        self.mean_stats_label.grid(row=4, column=1, padx=(0, 10), pady=5, sticky=W)
        Label(label_stats_frame, text='Median:').grid(row=5, column=0, padx=(10, 0), pady=5, sticky=W)
        self.median_stats_label = Label(label_stats_frame)
        self.median_stats_label.grid(row=5, column=1, padx=(0, 10), pady=5, sticky=W)
        Label(label_stats_frame, text='Std:').grid(row=6, column=0, padx=(10, 0), pady=5, sticky=W)
        self.std_stats_label = Label(label_stats_frame)
        self.std_stats_label.grid(row=6, column=1, padx=(0, 10), pady=5, sticky=W)
        Label(label_stats_frame, text='Var:').grid(row=7, column=0, padx=(10, 0), pady=5, sticky=W)
        self.var_stats_label = Label(label_stats_frame)
        self.var_stats_label.grid(row=7, column=1, padx=(0, 10), pady=5, sticky=W)

        self.histogram_stats_frame = Frame(stats_frame, bd=0, relief=GROOVE)
        self.histogram_stats_frame.rowconfigure(0, weight=1)
        self.histogram_stats_frame.columnconfigure(0, weight=1)
        self.histogram_stats_frame.grid(row=1, column=1, sticky=W+N+S+E)

    def create_output_frame(self, parent):
        output_frame = Frame(parent, bd=1, relief=GROOVE)
        output_frame.rowconfigure(0, weight=0)
        output_frame.rowconfigure(1, weight=1)
        output_frame.columnconfigure(0, weight=1)
        output_frame.grid(row=1, column=1, sticky=W+N+S+E)
        Label(output_frame, text='Console Output').grid(row=0, padx=10, pady=(10, 5), sticky=W)
        self.clear_log_button = Button(output_frame, text='Clear Logs')
        self.clear_log_button.grid(row=0, column=1, padx=10, pady=(10, 5), sticky=E)

        console_frame = Frame(output_frame)
        # self.console_text = Text(console_frame, height=5, state=DISABLED)
        self.console_text = Text(console_frame, height=5, state=DISABLED)
        scroll_bar = Scrollbar(console_frame)
        self.console_text.config(yscrollcommand=scroll_bar.set)
        scroll_bar.config(command=self.console_text.yview)

        console_frame.rowconfigure(0, weight=1)
        console_frame.columnconfigure(0, weight=1)
        console_frame.grid(row=1, columnspan=2, sticky=W+N+S+E)
        self.console_text.grid(column=0, sticky=W+N+S+E)
        scroll_bar.grid(column=1, row=0, sticky=W+N+S+E)








