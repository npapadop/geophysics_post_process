from tkinter import *

class ClipTrackView:
    def __init__(self, parentframe):
        self.clip_track_form_frame = Frame(parentframe)
        self.clip_track_form_frame.pack()

        self.minimum_value_str = StringVar()
        self.maximum_value_str = StringVar()
        self.maxmodelvalue = 'n/a'
        self.minmodelvalue = 'n/a'


    def create_form_widget(self):
        Label(self.clip_track_form_frame, text='Enter minimum data value:').grid(row=0, column=0, sticky=W)
        Entry(self.clip_track_form_frame, textvariable=self.minimum_value_str).grid(row=0, column=1, sticky=E+W, padx=5)
        Label(self.clip_track_form_frame, text="(min: {0})".format(self.minmodelvalue)).grid(row=0, column=2, sticky=E, padx=5)
        self.error_minimum_label = Label(self.clip_track_form_frame, text='This is an error', fg="red")
        self.error_minimum_label.grid(row=1, column=0, columnspan=2, sticky=E, padx=5, pady=(0, 5))

        Label(self.clip_track_form_frame, text='Enter maximum data value:').grid(row=2, column=0, sticky=W)
        Entry(self.clip_track_form_frame, textvariable=self.maximum_value_str).grid(row=2, column=1, sticky=E+W, padx=5)
        Label(self.clip_track_form_frame, text="(max: {0})".format(self.maxmodelvalue)).grid(row=2, column=2, sticky=W, padx=5)
        self.error_maximum_label = Label(self.clip_track_form_frame, text='This is an error', fg="red")
        self.error_maximum_label.grid(row=3, column=0, columnspan=2, sticky=E, padx=5, pady=(0,5))

        button_frame = Frame(self.clip_track_form_frame)
        button_frame.grid(row=4, column=0, columnspan=3, sticky=E)
        self.apply_button = Button(button_frame, text='Apply')
        self.apply_button.pack(side=RIGHT, padx=5, pady=10)
        self.cancel_button = Button(button_frame, text='Cancel')
        self.cancel_button.pack(side=RIGHT, padx=5, pady=10)