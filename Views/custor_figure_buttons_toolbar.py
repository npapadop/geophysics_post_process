from matplotlib.backends.backend_tkagg import NavigationToolbar2TkAgg


class CustomButtonsToolbar(NavigationToolbar2TkAgg):
    def __init__(self, canvas_, parent_):
        self.toolitems = (
            ('Home', 'Reset actions', 'home', 'home'),
            ('Pan', 'Move', 'move', 'pan'),
            ('Zoom', 'Zoom', 'zoom_to_rect', 'zoom'),
            ('Save', 'Save figure', 'filesave', 'save_figure'),
            )
        NavigationToolbar2TkAgg.__init__(self, canvas_, parent_)
