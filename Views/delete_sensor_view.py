from tkinter import *

class DeleteSensorView:
    def __init__(self, parentframe):
        self.delete_sensor_form_frame = Frame(parentframe)
        self.delete_sensor_form_frame.pack()
        self.sensor_list_choise = IntVar()
        self.track_list_choise = StringVar()
        self.sensor_option_list = []
        self.track_option_list = []

    def create_form_widget(self):
        Label(self.delete_sensor_form_frame, text='Select sensor to delete from all tracks:').grid(row=0, column=0, sticky=E)
        self.sensor_list_choise.set(self.sensor_option_list[0])
        self.option_sensor_list = OptionMenu(self.delete_sensor_form_frame, self.sensor_list_choise, *self.sensor_option_list)
        self.option_sensor_list.grid(row=0, column=1, sticky=E+W, padx=5)
        Label(self.delete_sensor_form_frame, text='Select which tracks should remove sensor data:').grid(row=1, column=0, sticky=E)
        self.track_list_choise.set(self.track_option_list[0])
        self.option_track_list = OptionMenu(self.delete_sensor_form_frame, self.track_list_choise, *self.track_option_list)
        self.option_track_list.grid(row=1, column=1, sticky=E+W, padx=5)
        button_frame = Frame(self.delete_sensor_form_frame)
        button_frame.grid(row=2, column=0, columnspan=2, sticky=E)
        self.apply_button = Button(button_frame, text='Apply')
        self.apply_button.pack(side=RIGHT, padx=5, pady=10)
        self.cancel_button = Button(button_frame, text='Cancel')
        self.cancel_button.pack(side=RIGHT, padx=5, pady=10)





