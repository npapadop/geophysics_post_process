from tkinter import *

class OutlierRemovalView:
    def __init__(self, parentview):
        self.outlier_removal_form_frame = Frame(parentview)
        self.outlier_removal_form_frame.pack()

        self.adjustment_value_str = StringVar()

    def create_form_widget(self):
        Label(self.outlier_removal_form_frame, text='Enter adjustment parameter value:').grid(row=0, column=0, sticky=W)
        Entry(self.outlier_removal_form_frame, textvariable=self.adjustment_value_str).grid(row=0, column=1, sticky=E + W, padx=5)
        self.error_parameter_label = Label(self.outlier_removal_form_frame, text='This is an error', fg="red")
        self.error_parameter_label.grid(row=1, column=0, columnspan=2, sticky=E, padx=5, pady=(0, 5))

        button_frame = Frame(self.outlier_removal_form_frame)
        button_frame.grid(row=2, column=0, columnspan=2, sticky=E)
        self.apply_button = Button(button_frame, text='Apply')
        self.apply_button.pack(side=RIGHT, padx=5, pady=10)
        self.cancel_button = Button(button_frame, text='Cancel')
        self.cancel_button.pack(side=RIGHT, padx=5, pady=10)