import numpy as np
import copy
import matplotlib.path as matplotpath
import Models.signal_process as signals
from shapely.geometry import MultiPoint
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from Observers.observer import Observable

class Model:
    def __init__(self):
        self.data_file_path = None
        self.data_file_name = None
        self.file_descriptor = None

        self.initial_data_file_records = []

        self.track_names = []
        self.tracks = []

        #data statistics
        self.sensor_values = []
        self.num_tracks = 0
        self.num_records = 0
        self.stats_amin = 0
        self.stats_amax = 0
        self.stats_mean = 0
        self.stats_median = 0
        self.stats_std = 0
        self.stats_var = 0

        self.clip_track_min = 0
        self.clip_track_max = 0

        self.adjustment_parameter = 0

        self.convex_hulls = []
        self.outlines = []
        self.polygons = []
        self.multipoints = []
        self.polygon_intersect_indexes = []

        self.remove_points_progress_perc = Observable('')
        self.process_remove_points_terminated = 0

        self.calibrate_values_progress_perc = Observable('')
        self.process_calibrate_values_terminated = 0

    '''
        Open file given as argument and returns the file descriptor
    '''
    def open_file(self, file_path, file_name):
        if file_path:
            with open(file_path, "r+") as self.file_descriptor:
                self.initial_data_file_records = []
                self.data_file_path = file_path
                self.data_file_name = file_name
                for line in self.file_descriptor:
                    self.initial_data_file_records.append(line)
            self.reload_initial_data()
        else:
            return None

    '''
        Read data from file and stores them to track structure
    '''
    def read_track_data(self, initial_records):
        file_line_no = 0

        for line in initial_records:
            file_line_no += 1

            split_line = line.split()
            track_name = split_line[3]
            sensor_id = int(split_line[4])
            line_record = [round(float(split_line[0][2:]), 3), round(float(split_line[1]), 3), round(float(split_line[2]), 3),
                           track_name, int(file_line_no)]

            if track_name not in self.track_names:
                self.track_names.append(track_name)
                sensor_list = [line_record]
                self.tracks.append([sensor_list])
            else:
                track_index = self.track_names.index(track_name)
                sensor_list = self.tracks[track_index]

                if len(sensor_list) >= sensor_id:
                    sensor_list[sensor_id - 1].append(line_record)
                else:
                    sensor_list.append([line_record])

    def reload_initial_data(self):
        self.track_names = []
        self.tracks = []
        self.read_track_data(list(self.initial_data_file_records))

    def calculate_data_stats(self):
        num_tracks = 0
        num_records = 0
        sensor_values = []

        for track in self.tracks:
            num_tracks += 1
            for sensor in track:
                num_records += len(sensor)
                for record in sensor:
                    sensor_values.append(record[2])

        self.num_tracks = num_tracks
        self.num_records = num_records
        self.sensor_values = sensor_values

        self.stats_amin = np.amin(self.sensor_values)
        self.stats_amax = np.amax(self.sensor_values)
        self.stats_mean = np.mean(self.sensor_values)
        self.stats_median = np.median(self.sensor_values)
        self.stats_std = np.std(self.sensor_values)
        self.stats_var = np.var(self.sensor_values)

    def delete_sensor(self, sensor_number, delete_type):
        if delete_type == 'all':
            for i in range(0, len(self.tracks)):
                self.tracks[i][sensor_number-1] = []
        elif delete_type == 'even':
            for i in range(1, len(self.tracks), 2):
                self.tracks[i][sensor_number - 1] = []
        elif delete_type == 'odd':
            for i in range(0, len(self.tracks), 2):
                self.tracks[i][sensor_number - 1] = []
        else:
            print("Wrong argument")

    def clip_track(self):
        for track in self.tracks:
            for sensor_list in track:
                for sensor_values in sensor_list:
                    if sensor_values[2] > self.clip_track_max:
                        sensor_values[2] = self.clip_track_max
                    elif sensor_values[2] < self.clip_track_min:
                        sensor_values[2] = self.clip_track_min

    def outlier_removal(self):
        for track_id in range(0, len(self.tracks)):
            track_sensors = self.tracks[track_id]
            for sensor_id in range(0, len(track_sensors)):
                    if len(self.tracks[track_id][sensor_id]) > 0:
                        x = np.array(self.tracks[track_id][sensor_id])
                        records = self.tracks[track_id][sensor_id]

                        fo = signals.func_despike_phasespace3d((x[:, 2]), 9, 2, self.adjustment_parameter)

                        fo_list = fo.tolist()
                        for rec_index in range(0, len(records)):
                            records[rec_index][2] = fo_list[rec_index]

    def write_track_data_from_planar(self, output_filename):
        file = open(output_filename, 'w')

        for track in self.tracks:
            for sensor in track:
                for record in sensor:
                    file.write(
                        "{0:.3f}\t{1:.3f}\t{2:.3f}\n".format(record[0], record[1], record[2]))

        file.close()

    def create_outline_polygons(self):
        self.outlines = []
        self.polygons = []

        for track_id in range(0, len(self.tracks)):
            first_sensor_values = []
            last_sensor_values = []
            first_record_list = []
            last_record_list = []

            for sensor_id in range(0, len(self.tracks[track_id])):
                if len(self.tracks[track_id][sensor_id]) > 0:
                    first_record_list = self.tracks[track_id][sensor_id]
                    break

            for sensor_id in range(len(self.tracks[track_id])-1, 0, -1):
                if len(self.tracks[track_id][sensor_id]) > 0:
                    last_record_list = self.tracks[track_id][sensor_id]
                    break

            for record_index in range(0, len(first_record_list)):
                first_sensor_values.append(
                    (first_record_list[record_index][0], first_record_list[record_index][1]))
                last_sensor_values.append(
                    (last_record_list[record_index][0], last_record_list[record_index][1]))

            outline = first_sensor_values + last_sensor_values[::-1]
            self.outlines.append(outline)

            polygon = matplotpath.Path(outline)
            self.polygons.append(polygon)

    def create_outline_convexhulls(self):
        self.convex_hulls = []

        for polygon_index in range(0, len(self.outlines)):
            hull = MultiPoint(self.outlines[polygon_index]).convex_hull
            self.convex_hulls.append(hull)

    def locate_ovelapping_tracks(self):
        self.polygon_intersect_indexes = []

        for convex_pri_id in range(0, len(self.convex_hulls)):
            first_convex = self.convex_hulls[convex_pri_id]

            for convex_sec_id in range(convex_pri_id+1, len(self.convex_hulls)):
                second_convex = self.convex_hulls[convex_sec_id]

                if first_convex.intersects(second_convex):
                    self.polygon_intersect_indexes.append([convex_pri_id, convex_sec_id])

    def calibrate_sensor_values_all(self, time, callback_when_calibration_done):
        self.process_calibrate_values_terminated = 0
        track_index = 0
        progress_perc = self.get_progress_bar_str(track_index, len(self.tracks), length=50)
        self.calibrate_values_progress_perc.set(progress_perc)

        num_tracks = 0

        for track in self.tracks:
            num_tracks += 1
            for sensor in track:
                for record in sensor:
                    if self.process_calibrate_values_terminated == 1:
                        return
                    record[2] -= self.stats_mean

            track_index += 1
            progress_perc = self.get_progress_bar_str(track_index, len(self.tracks), length=50)
            self.calibrate_values_progress_perc.set(progress_perc)

        callback_when_calibration_done()

    def calibrate_sensor_values_all_tracks(self, time, callback_when_calibration_done):
        self.process_calibrate_values_terminated = 0
        track_index = 0
        progress_perc = self.get_progress_bar_str(track_index, len(self.tracks), length=50)
        self.calibrate_values_progress_perc.set(progress_perc)

        num_tracks = 0
        max_sensor_num = (self.tracks[0]) and len(self.tracks[0]) or 0
        sensor_values = []
        for i in range(0, max_sensor_num):
            sensor_values.append([])

        for track in self.tracks:
            for sensor_i in range(0, len(track)):
                for record in track[sensor_i]:
                    sensor_values[sensor_i].append(record[2])

        for sensor_i in range(0, len(sensor_values)):
            if len(sensor_values[sensor_i]) > 0:
                sensor_values_mean = np.mean(sensor_values[sensor_i])
                sensor_values[sensor_i] = sensor_values_mean

        for track in self.tracks:
            num_tracks += 1
            sensor_i = 0
            for sensor in track:
                for record in sensor:
                    if self.process_calibrate_values_terminated == 1:
                        return
                    record[2] -= sensor_values[sensor_i]

                sensor_i += 1

            track_index += 1
            progress_perc = self.get_progress_bar_str(track_index, len(self.tracks), length=50)
            self.calibrate_values_progress_perc.set(progress_perc)

        callback_when_calibration_done()

    def calibrate_sensor_values_each_track(self, time, callback_when_calibration_done):
        self.process_calibrate_values_terminated = 0
        track_index = 0
        progress_perc = self.get_progress_bar_str(track_index, len(self.tracks), length=50)
        self.calibrate_values_progress_perc.set(progress_perc)

        num_tracks = 0
        num_records = 0
        sensor_values = []

        for track in self.tracks:
            num_tracks += 1
            for sensor in track:
                if len(sensor) > 0:
                    if self.process_calibrate_values_terminated == 1:
                        return

                    num_records += len(sensor)
                    for record in sensor:
                        sensor_values.append(record[2])

                    sensor_mean = np.mean(self.sensor_values)
                    for record in sensor:
                        record[2] -= sensor_mean

                    sensor_values = []

            track_index += 1
            progress_perc = self.get_progress_bar_str(track_index, len(self.tracks), length=50)
            self.calibrate_values_progress_perc.set(progress_perc)

        callback_when_calibration_done()

    def remove_ovelapping_points(self, time, callback_when_done):
        self.process_remove_points_terminated = 0
        pair_index = 0
        progress_perc = self.get_progress_bar_str(pair_index, len(self.polygon_intersect_indexes), length=50)
        self.remove_points_progress_perc.set(progress_perc)

        for intersect_pair in self.polygon_intersect_indexes:


            polygon = self.polygons[intersect_pair[0]]

            for sensor_index in range(0, len(self.tracks[intersect_pair[1]])):
                if self.process_remove_points_terminated == 1:
                    return

                if len(self.tracks[intersect_pair[1]][sensor_index]) > 0:
                    mask = polygon.contains_points(np.double(np.array(self.tracks[intersect_pair[1]][sensor_index])[:,:2]))

                    self.tracks[intersect_pair[1]][sensor_index] = [x for i, x in enumerate(self.tracks[intersect_pair[1]][sensor_index]) if not mask[i]]

            pair_index += 1
            progress_perc = self.get_progress_bar_str(pair_index, len(self.polygon_intersect_indexes), length=50)
            self.remove_points_progress_perc.set(progress_perc)

        callback_when_done()

    # Print iterations progress
    def get_progress_bar_str(self, iteration, total, decimals=0, length=100, fill='█'):
        str_bar = ''
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filled_length = int(length * iteration // total)
        bar = fill * filled_length + '--' * (length - filled_length)
        str_bar += "%s |%s| %s%% %s" % ("Progress: ", bar, percent, "completed")
        # Print New Line on Complete
        if iteration == total:
            str_bar += "\n"

        return str_bar

    def plot_outline_polygons(self):

        self.create_outline_polygons()

        plt.cla()
        ax = plt.subplot()

        for polygon in self.polygons:
            patch = mpatches.PathPatch(polygon, facecolor=None, edgecolor=None, alpha=0.5, zorder=2, label=polygon)
            ax.add_patch(patch)

        ax.axis('scaled')
        plt.show()

    def draw_data(self):
        index = 0
        colors = ['red', 'green', 'blue', 'black', 'pink', 'orange', 'yellow', 'grey']
        for track in self.tracks:
            x = []
            y = []
            color_index = index % 8

            for sensor in track:
                for record in sensor:
                    x.append(record[0])
                    y.append(record[1])
            plt.scatter(x, y, color=colors[color_index])
            plt.savefig('testplot.png')
            index += 1
        plt.show()
