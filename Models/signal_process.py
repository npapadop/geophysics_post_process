import numpy as np
import scipy.interpolate as spinterpolate

def func_despike_phasespace3d(x, i_plot, i_opt, adjustment_param):
    fi = np.copy(x.astype(float))

    #number of maximum iteration
    n_iter = 20
    n_out = 999

    n = len(fi)
    f_mean = np.mean(fi)
    f = fi - f_mean

    lambda_v = np.sqrt(2 * np.log(n));

    #loop
    n_loop = 1

    while n_out != 0 and n_loop <= n_iter:
        # --- main
        # step0
        f = f - np.nanmean(f)
        # step 1: first and second derivatives
        f_t = np.gradient(f)
        f_tt = np.gradient(f_t)

        # step 2: estimate angle between f and f_tt axis
        if n_loop == 1:
            theta = np.arctan2(np.sum(np.multiply(f, f_tt)), np.sum(f ** 2))

        # step 3: checking outlier in the 3D phase space
        xp, yp, zp, ip, coef = func_excludeoutlier_ellipsoid3d(f, f_t, f_tt, theta, adjustment_param)

        n_nan_1 = len(np.nonzero(np.isnan(f) == 1)[0])
        f[ip] = np.NaN
        n_nan_2 = len(np.nonzero(np.isnan(f) == 1)[0])

        n_out = n_nan_2 - n_nan_1

        n_loop += 1

    go = f + f_mean
    ip = np.nonzero(np.isnan(go))
    # --- interpolation or shorten NaN data
    fo = []

    if np.abs(i_opt) >= 1:
        # remove NaN from data
        inan = np.nonzero(np.logical_not(np.isnan(go)))
        fo = go[inan]

        if np.abs(i_opt) == 2:
            x = np.nonzero(np.logical_not(np.isnan(go)))[0]
            y = go[x]
            xi = np.arange(0, np.max(len(fi)))

            fo_first = spinterpolate.interp1d(x, y)
            fo = fo_first(xi)
    else:
        fo = np.copy(go)

    return fo


def func_excludeoutlier_ellipsoid3d(xi, yi, zi, theta, test_std):
    n = np.max(len(xi))
    lambda_v = np.sqrt(2*np.log(n))

    xp = []
    yp = []
    zp = []
    ip = []

    if theta == 0:
        X = np.copy(xi)
        Y = np.copy(yi)
        Z = np.copy(zi)
    else:
        R = np.array([[np.cos(theta), 0, np.sin(theta)], [0, 1, 0], [- np.sin(theta), 0, np.cos(theta)]])

        X = xi * R[0, 0] + yi * R[0, 1] + zi * R[0, 2]
        Y = xi * R[1, 0] + yi * R[1, 1] + zi * R[1, 2]
        Z = xi * R[2, 0] + yi * R[2, 1] + zi * R[2, 2]

    a = lambda_v * np.nanstd(X) * test_std
    b = lambda_v * np.nanstd(Y) * test_std
    c = lambda_v * np.nanstd(Z) * test_std

    # --- main

    m = 0

    for i in np.arange(1, n).reshape(-1):
        x1 = X[i]
        y1 = Y[i]
        z1 = Z[i]

        x2 = a * b * c * x1 / np.sqrt((a * c * y1) ** 2 + b ** 2 * (c ** 2 * x1 ** 2 + a ** 2 * z1 ** 2))
        y2 = a * b * c * y1 / np.sqrt((a * c * y1) ** 2 + b ** 2 * (c ** 2 * x1 ** 2 + a ** 2 * z1 ** 2))
        zt = c ** 2 * (1 - (x2 / a) ** 2 - (y2 / b) ** 2)

        if z1 < 0:
            z2 = - np.sqrt(zt)
        else:
            if z1 > 0:
                z2 = np.sqrt(zt)
            else:
                z2 = 0

        # check outlier from ellipsoid
        dis = (x2 ** 2 + y2 ** 2 + z2 ** 2) - (x1 ** 2 + y1 ** 2 + z1 ** 2)

        if dis < 0:
            ip.append(i)
            xp.append(xi[i])
            yp.append(yi[i])
            zp.append(zi[i])

    coef = [a, b, c]
    return xp, yp, zp, ip, coef
